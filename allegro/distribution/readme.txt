Installation Windows Allegro 4.4.2 avec addons png jpg ogg-vorbis et opengl

NOTE : il est conseill� d'utiliser l'utilitaire 7-Zip 64 bits 
� installer pr�alablement depuis https://www.7-zip.org/download.html

Pour rappel il est conseill� en tant qu'utilisateur "expert" de Windows de 
configurer l'explorateur de fichiers pour bien voir les extensions (.zip .jpg .exe etc...)
Dans une fen�tre d'explorateur de fichiers faire menu Fichier -> Options -> onglet Affichage
dans la liste Param�tres avanc�s : d�cocher les 2 cases de 
"Masquer les extensions des fichiers dont le type est connu" et
"Masquer les fichier prot�g�s du syst�me d'exploitation (recommand�)"
Cette derni�re manip rend Windows parano�aque : confirmez que vous voulez bien voir tout �a

T�l�charger allegro_4_4_install.zip depuis http://files.ece.fr/~fercoq/allegro/
V�rifier que dans l'explorateur de fichiers vous voyez bien l'extension .zip en v�rifiant le fichier obtenu

Lire attentivement la totalit� du paragraphe ci dessous avant de lancer la manip...

Le fichier allegro_4_4_install.zip doit �tre mis dans le r�pertoire MinGW, 
( usuellement C:\Program Files (x86)\CodeBlocks\MinGW )
et d�compress� directement � ce niveau (clic droit sur le zip -> 7-Zip -> "Extraire Ici")
de telle sorte que le contenu des r�pertoires bin, include et lib 
du zip se retrouvent dans les r�pertoire correspondants de MinGW.
Par exemple on v�rifie que allegro.h est bien dans MinGW\include (etc...)
Si des fichiers doivent �tre �cras�s lors de l'op�ration avec message 
"Confirmer le remplacement de fichier" alors cliquer le bouton "Non pour Tous"


Allegro 4.4.2 et ses addons est install� !
Cette proc�dure est d�finitive (elle n'est pas � refaire)

===========================================================================

Pour faire un projet Allegro dans CodeBlocks la proc�dure suivante sera toujours � refaire :
File -> New -> Project...  choisir  Console application
et suivre la proc�dure habituelle.

Pour compiler il faut ajouter des librairies dans les options de compilation :
Project -> Build options...
Dans la fen�tre [Project build options]
- S�lectionner le nom du projet en haut � gauche (au dessus de Debug et Release)
- Mettre en avant le panneau Linker settings
- Dans le cadre Link libraries: utiliser le bouton Add pour ajouter les librairies
  On ajoutera une ou plusieurs librairies selon les besoins d�crits ci apr�s
  ( En respectant l'ordre dans le cas o� on a plusieurs librairies )
- Valider (OK OK)

Pour compiler un projet basique (tous les exemples du cours)
alleg44.dll

Pour compiler un projet qui utilise des formats images compress�s png et jpg
loadpng
png16.dll
jpgalleg
alleg44.dll

Pour compiler un projet qui utilise le format son/musique compress� ogg-vorbis
logg
ogg.dll
vorbis.dll
vorbisfile.dll
alleg44.dll

Pour compiler un projet qui utilise OpenGL (AllegroGL)
alleggl
alleg44.dll
user32
gdi32
opengl32
glu32
