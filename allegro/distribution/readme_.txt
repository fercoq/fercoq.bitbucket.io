Installation Windows Allegro 4.4.2 avec addons png jpg ogg-vorbis et opengl

Le zip doit �tre d�compress� au niveau du r�pertoire MinGW, 
( usuellement C:\Program Files (x86)\CodeBlocks\MinGW )
de telle sorte que le contenu des r�pertoires bin, include et lib 
du zip se retrouvent dans les r�pertoire correspondants de MinGW.
Par exemple on v�rifie que allegro.h est bien dans MinGW\include (etc...)

Correctif 25/01 : L'outil CodeAnalysis (install� au 1er semestre) interf�re
avec le param�trage de CodeBlocks. Pour corriger ce probl�me il faut v�rifier
dans CodeBlocks menu d�roulant Settings -> Compiler... panneau Toolchain executables
Compiler's installation directory doit indiquer  
C:\Program Files (x86)\CodeBlocks\MinGW 
et non pas
C:\Program Files (x86)\CodeAnalysis Suite\Tools\MinGW
Le cas �ch�ant, indiquer le r�pertoire correct (en cliquant sur ... ou en �ditant)

Allegro 4.4.2 et ses addons est install� !

===========================================================================

Pour faire un projet Allegro dans CodeBlocks :
File -> New -> Project...  choisir  Console application
et suivre la proc�dure habituelle.

Pour compiler il faut ajouter des librairies dans les options de compilation :
Project -> Build options...
Dans la fen�tre [Project build options]
- S�lectionner le nom du projet en haut � gauche (au dessus de Debug Release)
- Mettre en avant le panneau Linker settings
- Dans le cadre Link libraries: utiliser le bouton Add pour ajouter les librairies
  On ajoutera une ou plusieurs librairies selon les besoins d�crits ci apr�s
  ( En respectant l'ordre dans le cas o� on a plusieurs librairies )
- Valider (OK OK)

Pour compiler un projet basique (tous les exemples du cours)
alleg44.dll

Pour compiler un projet qui utilise des formats images compress�s png et jpg
loadpng
png16.dll
jpgalleg
alleg44.dll

Pour compiler un projet qui utilise le format son/musique compress� ogg-vorbis
logg
ogg.dll
vorbis.dll
vorbisfile.dll
alleg44.dll

Pour compiler un projet qui utilise OpenGL (AllegroGL)
alleggl
alleg44.dll
user32
gdi32
opengl32
glu32
