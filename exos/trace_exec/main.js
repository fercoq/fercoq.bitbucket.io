/*
	Générateur d'exercice : entraînement aux traces d'exécution
	Version prototype initial, code brouillon à refactoriser
*/

/*
MIT licence :

Copyright 2020 Robin Fercoq

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

var nbVar = 3;
var varNames = ['X', 'Y', 'Z'];
var modeConditionals = false;

var chronoLevel = 0;
var chronoVal = 0;
var chronoTimer = false;
var chronoFoxTime = [30, 50];
var chronoTurtleTime = [60, 120];

var moodleMode = false;

function arbitraryArithmeticOperator()
{
	switch ( getRandomInt(0,2) )
	{
		case 0 : return '+'; break;
		case 1 : return '-'; break;
	}
}

function arbitraryComparisonOperator()
{
	switch ( getRandomInt(0,6) )
	{
		case 0 : return '=='; break;
		case 1 : return '!='; break;
		case 2 : return '<'; break;
		case 3 : return '<='; break;
		case 4 : return '>'; break;
		case 5 : return '>='; break;
	}
}

function arbitraryBooleanOperator()
{
	switch ( getRandomInt(0,2) )
	{
		case 0 : return '&&'; break;
		case 1 : return '||'; break;
	}
}

function arbitraryMinus()
{
	return getRandomInt(0, 2) ? '-' : '';
}

function arbitraryNumLiteral()
{
	var vals = [ 0, 0, 1, 1, 1, 2, 2, 3, 4, 5];
	//return '' + getRandomInt(-5, +6);
	var res = arbitraryMinus() + vals[ getRandomInt(0, vals.length) ];
	if ( res == '-0' ) res = '0';
	return res;
}

var assignedVars = [];
var usedVars = [];
var addUsedVars = [];

function resetAssignedVars()
{
	for (var i=0; i<nbVar; ++i)
		assignedVars[i] = false;
}

function countAssignedVars()
{
	let cptAssigned = 0;
	
	for (var i=0; i<nbVar; ++i)
		if ( assignedVars[i] )
			++cptAssigned;
		
	return cptAssigned;
}
		
function resetUsedVars()
{
	for (var i=0; i<nbVar; ++i)
		usedVars[i] = false;
}

function resetAddUsedVars()
{
	for (var i=0; i<nbVar; ++i)
		addUsedVars[i] = false;
}

function getUsedVar()
{
	let cptUsed = 0;
	
	for (var i=0; i<nbVar; ++i)
		if ( usedVars[i] || addUsedVars[i] )
			++cptUsed;
		
	if ( !cptUsed )
		return false;
	
	let iUsed=0;
	do	{
		iUsed = getRandomInt(0, nbVar);
	} while ( !(usedVars[iUsed] || addUsedVars[iUsed]) );

	return iUsed;
}

function getNotAddUsedVar()
{
	// Attention dangereux si toutes déjà addUsed !
	let iNUsed=0;
	do	{
		iNUsed = getRandomInt(0, nbVar);
	} while ( addUsedVars[iNUsed] );

	return iNUsed;
}


function addAddUsedVars()
{
	for (var i=0; i<nbVar; ++i)
		if ( addUsedVars[i] )
			usedVars[i] = true;
}

function removeUsedVar( iLval )
{
	usedVars[iLval] = false;
}


function arbitraryVar()
{
	let iVar = getNotAddUsedVar();
	addUsedVars[iVar] = true;
	return 'xVarJs[' + iVar + ']';
	//return 'xVarJs[' + getRandomInt(0, nbVar) + ']';
}

function reallyArbitraryVar()
{
	return 'xVarJs[' + getRandomInt(0, nbVar) + ']';
}


function arbitraryOperand(opType = Math.random() < 0.5)
{
	return opType ? arbitraryNumLiteral() : arbitraryVar();
}

function reallyArbitraryOperand(opType = Math.random() < 0.5)
{
	return opType ? arbitraryNumLiteral() : reallyArbitraryVar();
}


function arbitraryValueExpression()
{
	if ( Math.random() < 0.4 )
		return arbitraryOperand();
	
	var ops = [ [0,0], [0,1], [1,0] ] [getRandomInt(0,3)];
	
	var vExpr, op1, op2;
	do {
		vExpr =  (op1=arbitraryOperand(ops[0])) 
			   +      arbitraryArithmeticOperator()
			   + (op2=arbitraryOperand(ops[1]));
	} while ( op1 == op2 );

	var rep = [ ['+-', '-'], ['--', '+'], 
	            ['0+', ''],  ['0-', '-'], 
			    ['+0', ''],  ['-0', ''] ];
			 
	rep.forEach( function( rp ) {
		vExpr = vExpr.replace(rp[0], rp[1]);
	} );
	
	return vExpr;
}

function reallyArbitraryValueExpression()
{
	if ( Math.random() < 0.4 )
		return reallyArbitraryOperand();
	
	var ops = [ [0,0], [0,1], [1,0] ] [getRandomInt(0,3)];
	
	var vExpr, op1, op2;
	do {
		vExpr =  (op1=reallyArbitraryOperand(ops[0])) 
			   +      arbitraryArithmeticOperator()
			   + (op2=reallyArbitraryOperand(ops[1]));
	} while ( op1 == op2 );

	var rep = [ ['+-', '-'], ['--', '+'], 
	            ['0+', ''],  ['0-', '-'], 
			    ['+0', ''],  ['-0', ''] ];
			 
	rep.forEach( function( rp ) {
		vExpr = vExpr.replace(rp[0], rp[1]);
	} );
	
	return vExpr;
}


function arbitraryComparisonExpression()
{
	var vExpr;
	var templ = [ [0,0], [0,1], [1,0] ] [getRandomInt(0,3)];
	
	var ops = [ [0,0], [0,1], [1,0] ] [getRandomInt(0,3)];
	var op1, op2;
	var sameVar, noVar;
	do {
		vExpr =  (op1= templ[0] ? reallyArbitraryValueExpression() : reallyArbitraryOperand(ops[0])) 
			   +      arbitraryComparisonOperator()
			   + (op2= templ[1] ? reallyArbitraryValueExpression() : reallyArbitraryOperand(ops[1]));
			   
		//sameVar = op1[op1.indexOf('[')+1] == op2[op2.indexOf('[')+1];
		sameVar = false;
		var vr = [false, false, false];
		for (var i=0; i<op1.length; ++i)
			if ( op1.charAt(i)=='[' )
				vr[0+op1.charAt(i+1)]=true;
		for (var i=0; i<op2.length; ++i)
			if ( op2.charAt(i)=='[' )
				if ( vr[0+op2.charAt(i+1)] )
					sameVar = true;
			
		noVar = op1.indexOf('[')==-1 && op2.indexOf('[')==-1;
	} while ( op1==op2 || sameVar || noVar );

	if ( getRandomInt(0,4) )
		return vExpr;
	else
		return '!('+vExpr+')';
}

function arbitraryConditionalExpression()
{
	if ( getRandomInt(0,2) )
		return arbitraryComparisonExpression();
	
	if ( getRandomInt(0,3) )
		return    arbitraryComparisonExpression() + ' ' 
				+ arbitraryBooleanOperator()      + ' ' 
				+ arbitraryComparisonExpression();
	
	return   '!( ' + arbitraryComparisonExpression() + ' ' 
				+ arbitraryBooleanOperator()      + ' ' 
				+ arbitraryComparisonExpression() + ' )';
}

const condTemplates = 
[ 
	[ [0, 2, 'if'], [0, 0, '{'], [1, 2, '}'], [0, 2, 'end'] ],
	[ [0, 2, 'if'], [0, 0, '{'], [1, 2, '}'], [0, 0, 'else'], [0, 0, '{'], [1, 2, '}'], [0, 2, 'end'] ],
	[ [0, 2, 'if'], [0, 0, '{'], [1, 2, '}'], [0, 2, 'if'], [0, 0, '{'], [1, 2, '}'], [0, 2, 'end'] ],
	[ [0, 2, 'if'], [0, 0, '{'], [1, 2, '}'], [0, 2, 'if'], [0, 0, '{'], [1, 2, '}'], [0, 0, 'else'], [0, 0, '{'], [1, 2, '}'], [0, 2, 'end'] ],
	[ [0, 2, 'if'], [0, 0, '{'], [1, 2, '}'], [0, 0, 'else'], [0, 0, '{'], [1, 2, '}'], [0, 2, 'if'], [0, 0, '{'], [1, 2, '}'], [0, 2, 'end'] ],
];

function getIndent(indent)
{
	var sp = '';
	for (var i=0; i<4*indent; ++i)
		sp += ' ';
	return sp;
}

function arbitrarySequence(maxdepth=2, depth=0)
{
	var prog = [];
	var totalAssignments;
	//var nbTry = 20;
	
	do
	{
		totalAssignments = 0;
		resetAssignedVars();
		prog = [];
	
		var decl = 'int ' ;
		for (var i=0; i<nbVar; ++i)
			decl += 'xVarJs[' + i + ']' + ( i==nbVar-1 ? ';\n' : ', ' ) ;
		
		prog.push(decl);
		prog.push('\n');
		
		for (var i=0; i<nbVar; ++i)
			prog.push( 'xVarJs[' + i + '] = ' + arbitraryNumLiteral() + ';\n' );
		
		prog.push( '\n' );
		
		resetUsedVars();
		//var nbLines = modeConditionals ? getRandomInt(4, 9) : getRandomInt(4, 7);
		var nbLines = getRandomInt(4, 7);
		
		var ctIdx = getRandomInt(0, condTemplates.length);
		console.log( 'ctIdx : ' + ctIdx );
		var ctStep = 0;
		var nextLines = getRandomInt(condTemplates[ctIdx][ctStep][0], condTemplates[ctIdx][ctStep][1]+1);
		var indent = 0;
		
		for (var i=0; modeConditionals || i<nbLines; ++i)
		{
			if ( modeConditionals && nextLines==0 )
			{
				let what = condTemplates[ctIdx][ctStep][2];
				if ( what == 'end' )
					break;
				if ( what == 'if' )
					//what = 'if ( ' + 'true' + ' )';
					what = 'if ( ' + arbitraryConditionalExpression() + ' )';
				if ( what == '{' )
					++indent;
				if ( what == '}' )
					--indent;
				prog.push( what + '\n' );
				++ctStep;
				if ( ctStep == condTemplates[ctIdx].length )
					break;
				nextLines = getRandomInt(condTemplates[ctIdx][ctStep][0], condTemplates[ctIdx][ctStep][1]+1);
				continue;
			}
			
			let rval, iLval;
			
			do {
				resetAddUsedVars();
				rval = arbitraryValueExpression();
			} while (    ( iLval=getUsedVar() )===false 
					  || 'xVarJs[' + iLval + ']' ===  rval  );
			
			addAddUsedVars();
			removeUsedVar(iLval);
			assignedVars[iLval] = true;
			prog.push( getIndent(indent) + 'xVarJs[' + iLval + '] = ' + rval + ';\n' );
			++totalAssignments;
			//prog.push( arbitraryOperand(0) + ' = ' + arbitraryValueExpression() + ';\n' );
			--nextLines;
		}
		
	} while ( countAssignedVars()!==nbVar || totalAssignments>8 ); // && --nbTry);
	
	//if (!nbTry)		alert("nbTry exceeded");
	
	return prog;
}

function generate()
{
	modeConditionals = document.getElementById("cbConditionals").checked;
	console.log('modeConditionals : ' + modeConditionals);
	
	var prog = "Test";
	var trace = [];
		
	//var soluce = [1, 2, 3]; //{ X:1, Y:2, Z:3};
	//var conditions = { read:[], write:[] };
	
	prog = arbitrarySequence();
	
	//var progEvJs = prog.replace(/int/g, '');
	var progEvJs = prog.map( function( line ) {
		return line.replace(/int/g, '');
	} );
	
	progEvJs.forEach( function( line ) {
		console.log(line);
	} );
	
	
	var xVarJs = ['?', '?', '?'];
	
	var ifState = 0;
	var execute = true;
	
	for (var i=0; i<progEvJs.length; ++i)
	{
		var line = progEvJs[i];
		//if (line === '\n') continue;
		var res;
		
		//if ( ! (line.includes('if') || line.includes('else') || line.includes('{') || line.includes('}') ) )
		//	res = eval(line);
	
		if ( line.includes('if') )
		{
			const condition = line.replace('if', '');
			res = eval(condition);
			//console.log('condition ' + condition + ' => ' + res);
			
			ifState = 1;
			execute = res;
			trace.push( {values: xVarJs.slice(), execd: false, condition: res } );
			continue;
		}
		else if ( line.includes('}') )
		{
			if ( progEvJs[i+1]!==undefined && progEvJs[i+1].includes('else') )
			{
				trace.push( {values: xVarJs.slice(), execd: false, disabled: !execute } );
				continue;
			}
			trace.push( {values: xVarJs.slice(), execd: false, disabled: !execute } );
			ifState = 0;
			execute = true;
			continue;
		}
		else if ( line.includes('else') )
		{
			ifState = 2;
			execute = !execute;
			trace.push( {values: xVarJs.slice(), execd: false } );
			continue;
		}
		else if ( line.includes('{') )
		{
			trace.push( {values: xVarJs.slice(), execd: false, disabled: !execute } );
			continue;
		}
		
		if ( execute )
		{
			eval(line);
			trace.push( {values: xVarJs.slice(), execd: (line !== '\n') } );
		}
		else
		{
			trace.push( {values: xVarJs.slice(), execd: false, disabled: !execute } );
		}
		
		//console.log(trace.slice(-1)[0].execd);
		//console.log(line);
		//console.log(xVarJs);
	}
	
	//var res = eval(progEvJs);
	//console.log(xVarJs);
	
	// Pas terrible : à optimiser !
	varNames.forEach( function( vrn, i ) {
		//prog = prog.replace(new RegExp('xVarJs\\['+i+'\\]', 'g'), vrn);
		prog = prog.map( function(line) {
			return line.replace(new RegExp('xVarJs\\['+i+'\\]', 'g'), vrn);
		} );
	} );
	

	
	//return { prog: prog, soluce: soluce };
	return { prog, trace };
}

var soluce=[];
var res;
var firstAnswer = true;

function fillContent(show=false)
{
	const sth  = show ? "show-th" : "no-show-th";
	const std  = show ? "show-td" : "no-show-td";
	const stdl = show ? "show-td" : "show-td-line";
	
	var table = '';
	table += '<table><thead><tr><th class="'+sth+'">Code source</th>';
	if ( modeConditionals )
		table += '<th class="'+sth+'">' + 'Cond' + '</th>';
	varNames.forEach( function( vrn ) {
		table += '<th class="'+sth+'">' + vrn + '</th>';
	} );
	table += '</tr></thead>\n<tbody>\n';
	res.prog.forEach( function(line, i) {
		if ( show && res.trace[i].disabled===true )
			table += '<tr class="disabled-row">';
		else
			table += '<tr>';
		table += '<td class="'+stdl+'">';
		table += escapeHtml(line);
		table += '</td>';
		
		if ( modeConditionals )
		{
			table += '<td class="'+std+' num-value">';
			let cond = res.trace[i].condition;
			let disabled = res.trace[i].disabled;
			table += cond!==undefined ? ( cond ? 'vrai' : 'faux' ) : (disabled===true ? '&#8595; ' : '') ;
			table += '</td>';
		}
		for (var j=0; j<varNames.length; ++j)
		{
			table += '<td class="'+std+' num-value">';
			if ( res.trace[i].execd )
				table += '' + res.trace[i].values[j];
			//table += '<input id="trace_'+ i + '_' + j + '" value="?" size="3">';
			table += '</td>';
		}
		table += '</tr>\n';
	} );
	
	table += '</tbody></table>\n';
	
	setHTML("content", table );
}

function regenerate()
{
	//console.log("here we are !");
	
	//var prog = document.getElementById("content");
	//prog.innerHTML = "\tint X=0, Y=0, Z=0;\n\tif (X>3)\n\t\tZ=2;";
	
	//setHTML("content", "\tint X=0, Y=0, Z=0;\n\tif (X>3)\n\t\tZ=2;");
	
	res = generate();
	
	//console.log(res);
	
	//setHTML("content", res.prog.join('') );
	
	fillContent();
	
	//console.log("sol1 :");
	
	setHTML("soluce", '\t' 
					+ 'X vaut ' + '<input id="answ0" class="answer-input" placeholder="?" size="3"> &nbsp;&nbsp;&nbsp;' 
	                + 'Y vaut ' + '<input id="answ1" class="answer-input" placeholder="?" size="3"> &nbsp;&nbsp;&nbsp;' 
                    + 'Z vaut ' + '<input id="answ2" class="answer-input" placeholder="?" size="3"> &nbsp;&nbsp;&nbsp;'  );
	

	setHTML("resultat", "");

	/* ???
	const nodes = document.getElementsByClassName("answer-input");
	nodes.forEach( function(node) {
		node.addEventListener("keydown", function(event) {
			if (event.key === "Enter") {
				event.preventDefault();
				// Do more work
				showSoluce();
			}
		} );
	} );
	*/

	for (var i=0; i<3; ++i)
	{
		const node = document.getElementById("answ"+i);
		node.addEventListener("keydown", function(event) {
			if (event.key === "Enter") {
				event.preventDefault();
				// Do more work
				checkAnswer();
			}
		});
	}
	
	//soluce = res.soluce;
	soluce = res.trace.slice(-1)[0].values.slice();
	console.log("soluce :");
	console.log(soluce);
	
	if ( chronoTimer )
		clearInterval( chronoTimer );
	
	chronoVal = 0;
	setHTML("chrono", chronoVal);
	document.getElementById("chrono-field").style.backgroundImage = 'url("../icons/hare.png")';
		
	chronoLevel = modeConditionals ? 1 : 0;
	
	chronoTimer = setInterval( function(){ 
		setHTML("chrono", ++chronoVal);
		if ( chronoVal == chronoFoxTime[chronoLevel] )
			document.getElementById("chrono-field").style.backgroundImage = 'url("../icons/fox.png")';
		if ( chronoVal == chronoTurtleTime[chronoLevel] )
			document.getElementById("chrono-field").style.backgroundImage = 'url("../icons/turtle.png")';
	}, 1000);
	
	firstAnswer = true;
}

function getChronoAnimal()
{
	if ( chronoVal < chronoFoxTime[chronoLevel] )
		return 'hare';
	
	if ( chronoVal < chronoTurtleTime[chronoLevel] )
		return 'fox';
	
	return 'turtle';
}


function checkAnswer()
{
	var cptResult=0;
	cptResult += checkValue("answ0", soluce[0]);
	cptResult += checkValue("answ1", soluce[1]);
	cptResult += checkValue("answ2", soluce[2]);
	
	if ( cptResult == nbVar )
	{
		setStyleColor("resultat", "#00A000");
		setHTML("resultat", "PARFAIT !");
	}
	else
	{
		setStyleColor("resultat", "#A00000");
		setHTML("resultat", "" + cptResult + "/3" );
	}
	
	if ( firstAnswer )
	{	
		const progress = document.getElementById("progression");
		progress.innerHTML += '<div style="display: table-cell; text-align: center;"><img src="../icons/' 
		+ (cptResult == nbVar ? 'green_tick' : 'red_cross')
		+'.png"><br><img src="../icons/' + getChronoAnimal() +'.png" width="75px" height="45px"></div>';
	}
	
	firstAnswer = false;
}

function showSoluce()
{
	/*
	setHTML("soluce", '\t' + 'X vaut ' + soluce[0] + '   ' 
	                       + 'Y vaut ' + soluce[1] + '   '
                           + 'Z vaut ' + soluce[2] + '   ' );
	*/
	
	
	//setStyleColor("answ0", '#ff0000'); 
	
	checkAnswer();
	
	fillContent(true);
	
	/*
	res.prog.forEach( function(line, i) {		
		for (var j=0; j<varNames.length; ++j)
		{
			var id = 'trace_'+ i + '_' + j;
			setValue(id, res.trace[i].values[j]);
		}
	} );
	*/
}


function main()
{
	// https://www.sitepoint.com/get-url-parameters-with-javascript/
	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	moodleMode = urlParams.has('moodle');
	//console.log('moodle mode : ' + moodleMode);
	
	if ( moodleMode )
	{
		const navbuts = document.getElementById("nav-buttons");
		navbuts.innerHTML += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	<input type="number" id="moodle-nb-quiz" size="4" value="400">';
		navbuts.innerHTML += '&nbsp;&nbsp;<div id="moodle-block" style="display: inline-block;"><button onclick="downloadMoodle()">MOODLE...</button></div>';
	}
	
	regenerate();
}

function downloadMoodle()
{
	document.getElementById("moodle-block").innerHTML += '&nbsp;&nbsp;<a id="download_link" download="moodle_quiz_file.txt" href=”” >Download moodle quiz file</a>';
	
	const nbQuiz = document.getElementById("moodle-nb-quiz").value;
	//var text = 'test ' + nbQuiz;
	var text = '// Todo : format XML de quiz Moodle, sera à régler par essais/erreurs !\n\n<?xml version="1.0" ?>\n<quiz>\n';
	
	for (var i=0; i<nbQuiz; ++i)
	{
		const exo = generate();
		text += 
`\n\n\n<question type="shortanswer">
     <name>
         <text>tracer l'exécution...</text>
     </name>
     <questiontext format="html">
        <text>\n`;
		
		text += exo.prog.join('');
		
		text += 
`        </text>
     </questiontext>`;
	 
		text += 
`\n	<answer fraction="100">
		<text>\n`;
		
		var answer = exo.trace.slice(-1)[0].values.slice();
		
		text += 'X=' + answer[0] + ' Y=' + answer[1] + ' Z=' + answer[2] + '\n';
		
		text += 
`		</text>
		<feedback><text>Correct!</text></feedback>
	</answer>
	
 </question>`;
	}
	
	text += '\n\n</quiz>';
	
	var data = new Blob([text], {type: 'text/plain'});

	var url = window.URL.createObjectURL(data);
	document.getElementById('download_link').href = url;
}

// Helpers
function setHTML(id, html)
{
	document.getElementById(id).innerHTML = ''+html;
}

function setValue(id, val)
{
	document.getElementById(id).value = val;
}

function checkValue(id, val)
{
	var elem = document.getElementById(id);
	var preval = elem.value;
	
	if ( ""+val === preval )
	{
		elem.style.color = '#00A000';
		return 1;
	}
	
	elem.style.color = '#A00000';
	return 0;
	//document.getElementById(id).value = val;
}

function setStyleColor(id, color)
{
	document.getElementById(id).style.color = color;
}


function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}


// https://stackoverflow.com/a/6234804
function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }


