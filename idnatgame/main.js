/*
	species identification trainer/game using inaturalist ressources
	very early preliminary prototype to learn and test only %)
	
	⚠<i> subject to occasionnal misidentifications </i>⚠
	this indie webapp is not endorsed by inaturalist.org
*/

/*
MIT licence :

Copyright 2021 Robin Fercoq

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

"use strict";

/********************************** */
/** TUNABLE PARAMETERS OF THE GAME  */
/********************************** */

const taxons = [ 'Tanacetum vulgare', 'Oenothera biennis', 'Cirsium oleraceum', 
                 'Digitalis purpurea', 'Epilobium hirsutum', 'Calopteryx splendens', 
				 'Hypholoma fasciculare', 'Helix pomatia', 'Sambucus ebulus', 
				 'Cicerbita plumieri', 'Angelica sylvestris', 'Cornus mas', 
				 'Melolontha melolontha', 'Solanum dulcamara' ];


const locale = 'fr';

const countObservations = 4;

/***************************************** */
/** END OF TUNABLE PARAMETERS OF THE GAME  */
/***************************************** */


const fetchTaxonCountUrl = 'https://api.inaturalist.org/v1/observations/species_counts?quality_grade=research&locale='+locale;

const fetchTaxonNthObservationUrl = 'https://api.inaturalist.org/v1/observations?quality_grade=research&locale='+locale+'&per_page=1&order=desc&order_by=created_at';

const taxonUrlParam = taxon => '&taxon_name=' + encodeURI(taxon);
const nthPageUrlParam = n => '&page=' + n;

async function fetchTaxonData(taxon) {
	const response = await fetch(fetchTaxonCountUrl + taxonUrlParam(taxon));

	if (!response.ok) {
		throw new Error(`HTTP error! status: ${response.status}`);
	} else {
		const text = await response.text();
		const data = JSON.parse( text ).results[0];
		return [data.count, data.taxon.preferred_common_name];
	}
}

async function fetchTaxonNthObservationPics(taxon, n) {
	const response = await fetch(    fetchTaxonNthObservationUrl 
                                 + taxonUrlParam(taxon) + nthPageUrlParam(n)  );

	if (!response.ok) {
		throw new Error(`HTTP error! status: ${response.status}`);
	} else {
		const text = await response.text();
		return JSON.parse( text ).results[0].photos;
	}
}

async function fetchTaxonNthObservationRandomPic(taxon, n) {
	const pics = await fetchTaxonNthObservationPics(taxon, n);
	const picUrl = getRandomElement(pics).url.replace('square', 'medium');
	console.log(picUrl);
	const image = new Image();

	image.crossorigin = 'anonymous';
	image.src = picUrl;
	
	// https://stackoverflow.com/a/64747517
	await image.decode();
	
	return image;
}

/// Returns an array of count unique random values in [0...max-1]
function randomIdx(count, max)
{
	const idx = new Map();

	while ( idx.size != count )
		idx.set( getRandomInt(max), 0 );
	
	return [ ...idx.keys() ];
}

async function regenerate()
{
	console.log("here we are !");	
	
	setHTML('commonName', '');
	setHTML('latinName', '');
	let lookFor = getRandomElement(taxons);
	
	let [taxonCount, taxonCommonName] = await fetchTaxonData(lookFor);
	
	console.log(taxonCount);
	
	if (countObservations >= taxonCount)
		throw new Error(`Logic error ! For taxon "${lookFor}" countObservations=${countObservations} >= taxonCount=${taxonCount}`);
	
	// XHRGEThttps://api.inaturalist.org/v1/observations?quality_grade=research&locale=fr&per_page=1&order=desc&order_by=created_at&taxon_name=Digitalis%20purpurea&page=11215
	// [HTTP/1.1 403 Forbidden 201ms]

	if ( taxonCount>9999 ) taxonCount=9999;
	
	const randObsIdx = randomIdx(countObservations, taxonCount);
	console.log( randObsIdx );
	
	const promises = Array.from( randObsIdx, idx => fetchTaxonNthObservationRandomPic(lookFor, idx));
	
	const pics = await Promise.all( promises );
	
	//pics.forEach( pic => document.body.appendChild(pic) );
	pics.forEach( pic => {
		const div = document.createElement('div');
		div.className = 'observationFrame';
		pic.className = 'observationImg';
		div.appendChild(pic);
		/*
		const centering = document.createElement('div');
		centering.className = 'centering';
		div.appendChild(centering);
		centering.appendChild(pic);
		*/
		document.getElementById('pictures').appendChild(div);
	} );
	
	setHTML('commonName', '?');
	await sleep(5000);
	setHTML('commonName', taxonCommonName);
	setHTML('latinName', lookFor);
	//await sleep(5000);
	//location.reload();
}

function main()
{
	
	// //document.body...
	document.addEventListener('click', function(){ 
		//if ( event.target != document.getElementById("linkinat") ) // ???
		    location.reload();
		} , false);
	
	
	regenerate()
	.catch(e => {
		console.log('There has been a problem with your regenerate operation: ' + e.message);
	});
	
	console.log("end of main !");
}



// Helpers

// https://stackoverflow.com/a/39914235
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function setHTML(id, html)
{
	document.getElementById(id).innerHTML = ''+html;
}

function setValue(id, val)
{
	document.getElementById(id).value = val;
}

function checkValue(id, val)
{
	var elem = document.getElementById(id);
	var preval = elem.value;
	
	if ( ""+val === preval )
	{
		elem.style.color = '#00A000';
		return 1;
	}
	
	elem.style.color = '#A00000';
	return 0;
	//document.getElementById(id).value = val;
}

function setStyleColor(id, color)
{
	document.getElementById(id).style.color = color;
}

function getRandomElement(tab)
{
	return tab[getRandomInt(tab.length)];
}

function getRandomInt(min, max) {
	if (arguments.length==1)
	{
		max = min;
		min = 0;
	}
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}


// https://stackoverflow.com/a/6234804
function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }


	/*
	promises.push( fetchTaxonNthObservationRandomPic(lookFor, 
	                                               getRandomInt(taxonCount) ) );
	promises.push( fetchTaxonNthObservationRandomPic(lookFor, 
	                                               getRandomInt(taxonCount) ) );
												   
	const pics = await Promise.all( promises );
	
	document.body.appendChild(pics[0]);
	document.body.appendChild(pics[1]);
	*/
									
	/*
	const promisedPic = fetchTaxonNthObservationRandomPic(lookFor, 
	                                               getRandomInt(taxonCount) );
	const pics = await promisedPic;
	document.body.appendChild(pic);
	*/
	
	//console.log(pics.length);
	//console.log(pics);