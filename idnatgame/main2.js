/*
	species identification trainer/game using inaturalist ressources
	very early preliminary prototype to learn and test only %)
*/

/*
MIT licence :

Copyright 2021 Robin Fercoq

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

"use strict";

const lookFor = 'Allium carinatum';
const locale = 'fr';

///https://api.inaturalist.org/v1/observations/species_counts?taxon_name=Allium%20carinatum&quality_grade=research
const fetchTaxonCountUrl = 'https://api.inaturalist.org/v1/observations/species_counts?quality_grade=research&locale='+locale;

const fetchTaxonNthObservationUrl = 'https://api.inaturalist.org/v1/observations?quality_grade=research&locale='+locale+'&per_page=1&order=desc&order_by=created_at';

const taxonUrlParam = taxon => '&taxon_name=' + encodeURI(taxon);
const nthPageUrlParam = n => '&page=' + n;

async function fetchTaxonCount(taxon) {
	const response = await fetch(fetchTaxonCountUrl + taxonUrlParam(taxon));

	if (!response.ok) {
		throw new Error(`HTTP error! status: ${response.status}`);
	} else {
		const text = await response.text();
		return JSON.parse( text ).results[0].count;
	}
}

async function fetchTaxonNthObservationPics(taxon, n) {
	const response = await fetch(    fetchTaxonNthObservationUrl 
                                 + taxonUrlParam(taxon) + nthPageUrlParam(n)  );

	if (!response.ok) {
		throw new Error(`HTTP error! status: ${response.status}`);
	} else {
		const text = await response.text();
		return JSON.parse( text ).results[0].photos;
	}
}

async function fetchTaxonNthObservationRandomPic_(taxon, n) {
	const pics = await fetchTaxonNthObservationPics(taxon, n);
	const picUrl = getRandomElement(pics).url.replace('square', 'medium');
	//const picUrl = pics[0].url.replace('square', 'medium');
			   
	console.log(picUrl);
	//setHTML('picture', '<img src="' + picUrl + '">');
	
	//const image = document.createElement('img');
	const image = new Image();
	image.crossorigin = 'anonymous';
	image.src = picUrl;
	//image.addEventListener('click', event => { console.log('Button Clicked'); });
	image.addEventListener('load', async event => { 
		console.log('Loaded ' + image.naturalWidth + 'x' + image.naturalHeight); 
		await sleep(2000);
		document.body.appendChild(image);
	});
	//document.body.appendChild(image);
	
	/*
	const response = await fetch(picUrl, {mode: 'no-cors'});
	//const response = await fetch(picUrl, {mode: 'cors'});
	//const response = await fetch(picUrl);
	
	if (!response.ok) {
		throw new Error(`HTTP error! status: ${response.status}`);
	} else {
		const blob = await response.blob();

		const objectURL = URL.createObjectURL(blob);
		const image = document.createElement('img');
		image.src = objectURL;
		document.body.appendChild(image);
	}
	*/
	
}

async function fetchTaxonNthObservationRandomPic(taxon, n) {
	const pics = await fetchTaxonNthObservationPics(taxon, n);
	const picUrl = getRandomElement(pics).url.replace('square', 'medium');
	console.log(picUrl);
	const image = new Image();

	image.crossorigin = 'anonymous';
	image.src = picUrl;
	
	// https://stackoverflow.com/a/64747517
	await image.decode();
	
	return image;
}

async function regenerate()
{
	console.log("here we are !");	
	
	const taxonCount = await fetchTaxonCount(lookFor);
	
	console.log(taxonCount);
	
	const promises = [];
	promises.push( fetchTaxonNthObservationRandomPic(lookFor, 
	                                               getRandomInt(taxonCount) ) );
	promises.push( fetchTaxonNthObservationRandomPic(lookFor, 
	                                               getRandomInt(taxonCount) ) );
												   
	const pics = await Promise.all( promises );
	
	document.body.appendChild(pics[0]);
	document.body.appendChild(pics[1]);
									
	/*
	const promisedPic = fetchTaxonNthObservationRandomPic(lookFor, 
	                                               getRandomInt(taxonCount) );
	const pics = await promisedPic;
	document.body.appendChild(pic);
	*/
	
	//console.log(pics.length);
	//console.log(pics);
}

function main()
{
	
	regenerate()
	.catch(e => {
		console.log('There has been a problem with your regenerate operation: ' + e.message);
	});
	
	console.log("end of main !");
}


// Helpers
// https://stackoverflow.com/a/39914235
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


function setHTML(id, html)
{
	document.getElementById(id).innerHTML = ''+html;
}

function setValue(id, val)
{
	document.getElementById(id).value = val;
}

function checkValue(id, val)
{
	var elem = document.getElementById(id);
	var preval = elem.value;
	
	if ( ""+val === preval )
	{
		elem.style.color = '#00A000';
		return 1;
	}
	
	elem.style.color = '#A00000';
	return 0;
	//document.getElementById(id).value = val;
}

function setStyleColor(id, color)
{
	document.getElementById(id).style.color = color;
}

function getRandomElement(tab)
{
	return tab[getRandomInt(tab.length)];
}

function getRandomInt(min, max) {
	if (arguments.length==1)
	{
		max = min;
		min = 0;
	}
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}


// https://stackoverflow.com/a/6234804
function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }


