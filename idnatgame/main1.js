/*
	species identification trainer/game using inaturalist ressources
	very early preliminary prototype to learn and test only %)
*/

/*
MIT licence :

Copyright 2021 Robin Fercoq

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

"use strict";

const species = 'Allium carinatum';
const locale = 'fr';

const baseApi = 'https://api.inaturalist.org/v1/observations?quality_grade=research&locale='+locale+'&per_page=1&order=desc&order_by=created_at';

function loadXMLDoc2(getUrl) {
	
	console.log("getting :" + getUrl);

	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
		   if (xmlhttp.status == 200) {
			   //document.getElementById("myDiv").innerHTML = xmlhttp.responseText;
			   //console.log("got : " + xmlhttp.responseText);
			   const answer = JSON.parse( xmlhttp.responseText );
			   console.log(answer.results[0].photos.length);
			   
			   const iphoto = getRandomInt(0, answer.results[0].photos.length);
			   const photoUrl = answer.results[0].photos[ iphoto ].url;
			   
			   console.log(photoUrl);
			   setHTML('picture', '<img src="' + photoUrl.replace('square', 'large') + '">');
		   }
		   else {
			  alert('There was an error ' + xmlhttp.status);
		   }
		}
	};

	xmlhttp.open("GET", getUrl, true);
	xmlhttp.send();
}


function loadXMLDoc(getUrl) {
	
	console.log("getting :" + getUrl);
	
	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
		   if (xmlhttp.status == 200) {
			   //document.getElementById("myDiv").innerHTML = xmlhttp.responseText;
			   //console.log("got : " + xmlhttp.responseText);
			   const answer = JSON.parse( xmlhttp.responseText );
			   console.log("got : " + answer.total_results);
			   const iobs = getRandomInt(0, answer.total_results);
			   //const iobs = 0;
			   loadXMLDoc2( baseApi + '&taxon_name=' + encodeURI(species) + '&page=' + iobs);
		   }
		   else {
			  alert('There was an error ' + xmlhttp.status);
		   }
		}
	};

	xmlhttp.open("GET", getUrl, true);
	xmlhttp.send();
}

function regenerate()
{
	console.log("here we are !");	
	
	const getUrl = baseApi + '&taxon_name=' + encodeURI(species) + '&page=1';
	loadXMLDoc(getUrl);
}

function main()
{
	/*
	// https://www.sitepoint.com/get-url-parameters-with-javascript/
	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	thingMode = urlParams.has('thing');
	*/
	
	regenerate();
}


// Helpers
function setHTML(id, html)
{
	document.getElementById(id).innerHTML = ''+html;
}

function setValue(id, val)
{
	document.getElementById(id).value = val;
}

function checkValue(id, val)
{
	var elem = document.getElementById(id);
	var preval = elem.value;
	
	if ( ""+val === preval )
	{
		elem.style.color = '#00A000';
		return 1;
	}
	
	elem.style.color = '#A00000';
	return 0;
	//document.getElementById(id).value = val;
}

function setStyleColor(id, color)
{
	document.getElementById(id).style.color = color;
}


function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}


// https://stackoverflow.com/a/6234804
function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }


